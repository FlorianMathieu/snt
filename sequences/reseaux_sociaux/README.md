> Parmi les outils du web que nous utilisons le plus, les réseaux sociaux ont une très bonne place.
>
> Facebook, Twitter, Instagram, TikTok...font partie des sites et applications les plus employés quotidiennement.
>
> Mais au fait, comment en est-on arrivés là ?

## Historique

[Sur cette frise, vous pourrez voir un bref historique des réseaux sociaux.](https://view.genial.ly/6267e8fd4d958600180aa861/interactive-content-color-edu-timeline-fr)

Internet, le téléphone, le télégramme...toutes ces invention ont un point commun : permettre à l'humain de communiquer avec ses semblables.

































Sources :

>  Par Laurens van Lieshout — Travail personnel, Domaine public, https://commons.wikimedia.org/w/index.php?curid=2032585

> David R. Woolley & Doug Brown, Public domain, via Wikimedia Commons

> Par Elaine Chan and Priscilla Chan — http://harvard.facebook.com/photo.php?pid=30054432&amp;id=4&amp;l=ae012, CC BY 2.5, https://commons.wikimedia.org/w/index.php?curid=1779625

> Par Snapchat — https://www.snap.com/fr-FR/brand-guidelines/, marque déposée, https://fr.wikipedia.org/w/index.php?curid=10709617