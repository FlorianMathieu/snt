# Historique et archives du web



> Objectif de la séance : découvrir comment le web a évolué au cours des années, et analyser comment le regard de la société sur cette technologie a évolué.



Si aujourd'hui, Internet et le Web font intégralement partie de votre vie, de votre quotidien, avant votre naissance, ça n'était pas tout à fait la même chose... (et oui, il y a des gens qui sont plus vieux que vous ! )

À travers plusieurs documents vidéo, vous allez découvrir comment le web s'est construit une image sulfureuse, avant de devenir un outil dont on ne peut se passer.

--------

Les différentes activités sont à réaliser par binôme, votre compte-rendu sera effectué sur un document word ou libre office.

-----------------

## Activité 1 



Regardez la vidéo suivante et répondez aux questions ci dessous.

Rappel : l'utilisation d'écouteurs est fortement conseillée !

[Internet vu par la France dans les années 90](Internet%20-%20Il%20y%20a%2015%20ans.mp4)



***Question n° 1*** :

*Citer deux causes à la difficulté de mise en place d'Internet en France dans les années 90*

***Question n° 2 :***

*Quelle est la langue officielle d'Internet ?*

***Question n° 3***

*Comment appelait-on l'appareil permettant de se connecter à Internet à l'époque ?*

***Question n°4*** 

*Le web est il aussi libre en 2022 qu'il ne l'était en 1995 ? Pourquoi ?*

---------------------------

## Activité 2 

Regardez la vidéo suivante, et répondez aux questions ci dessous .

[Debut_Internet](Debut_Internet.mp4)



***Question n° 1***

*Les réseaux P2P étaient courants dans les années 90 et 2000. Pour quels types d'usage ?*

***Question n° 2***

*Quel était le support de données le plus utilisé à cette époque ? Combien cela représente t-il de données ?*

*Supposons un débit de base de 100 mbits /s, combien de temps vous aurez t-il fallu pour télécharger l'équivalent d'une disquette ?*

***Question n°3***

*Les outils de communications ou de messagerie instantanée étaient nombreux dans les années 2000. Citez trois (3) outils différents d'après vos connaissances ou vos recherches Internet.*



-----------------



## Activité 3

Même consigne que les exercices du dessus.

[Minitel](Minitel.mp4)



![Minitel](../assets/Minitel.jpeg)



***Question n° 1***

*Qu'est ce que le Minitel ?*

***Question n° 2*** 

*Avec quel réseau fonctionnait-il ?*

*Quel type de connexion, que vous connaissez, utilise ou utilisait le même réseau ?*

***Question n° 3***

*Quel type de services fournissait-il ?*

***Question n° 4***

*Quelle grande personnalité de l'Internet français a fait fortune dans le Minitel ?*