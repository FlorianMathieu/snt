# Activité - Simulation de Réseau

## Présentation

La présente activité a pour objectif de faire construire, faire configurer et étudier l'ensemble des notions gravitant autour d'Internet, via le simulateur __Filius__.

L'activité est progressive et se découpe en 4 séances :

1. [Construire un réseau simple](./seance_1/)
2. [Connecter deux réseaux](./seance_2/)
3. [Accéder à une page web...ou l'utilité d'un DNS](./seance_3/)
4. [Échanger des fichiers dans un réseau pair-à-pair](./seance_4/)

