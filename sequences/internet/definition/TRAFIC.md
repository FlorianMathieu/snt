# Trafic Internet et Bande Passante

## Documents

### Document 1 - De plus en plus de trafic sur internet

<img src="./assets/trafic.png" alt="Volume mensuel du trafic d'Internet, Source : Cisco" style="zoom:50%;" />

<img src="https://cdn.statcdn.com/Infographic/images/normal/21207.jpeg" alt="Distribution du trafic en fonction des usages en 2019, Source : Statista" style="zoom: 33%;" />

\newpage

### Document 2 - Des débits toujours croissants

|   Mode de Transmission   |     Type de connectique     |   Débits constatés    | Remarques                                                    |
| :----------------------: | :-------------------------: | :-------------------: | :----------------------------------------------------------- |
| Modem | Câble (réseau téléphonique) | 48 Kbit/s | Débit en vigueur au début des années 2000. |
| Fibre optique domestique |    Câble (Fibre optique)    | 300 Mbit/s à 1 Gbit/s | Mieux développé dans les grandes villes                      |
|           ADSL           | Câble (réseau téléphonique) |     1 à 60 Mbit/s     | Passe par le réseau téléphonique déjà installé, très courant aujourd’hui. |
|  Réseaux câbles urbains  |       Câble (cuivre)        |      600 Mbit/s       | Technologie basée sur l’ancien réseau de télévision par câble. |
|            4G            |          Sans fil           |    30 à 100 Mbit/s    | A vocation à être remplacé par la 5G                         |
|            5G            |          Sans fil           |     1 à 10 Gbit/s     | En cours de déploiement                                      |
|        Satellite         |          Sans fil           |       20 Mbit/s       | Couvre la France entière sans « zone d’ombre »               |

<img src="./assets/adsl.png" alt="Exemple de test de bande passante ADSL, source : https://freebox-news.com/tutoriel/test-debit-free" style="zoom:50%;" />

### Document 3 - Unités de mesure 

**L’unité élémentaire** : En informatique l’unité élémentaire de mesure en informatique est le bit. Une information de 1 bit ne peut prendre que deux valeurs : 0 ou 1.

**L’unité de base** : L’unité de base est l’octet, noté o. Un octet correspond à 8 bits.

**Les multiples** : Avant 1998, 1 kilo représentait 1024 octets permettant de retrouver facilement les puissance de 2 ($2^{10} = 1024$). Depuis 1998, l’IEC a statué et maintenant : 1 ko = 1000 o, 1 Mo = 1000 ko, …

Toutefois, pour ne pas bouleverser les usages, la commission a introduit de nouveau préfixe binaires : le kibi (noté ki), le mébi (noté Mi), …. permettant de retrouver les puissances de 2. Ainsi 1 kio = 1024 o, 1 Mio = 1024 kio, …

Calcul bande passante : $`BP = \frac{Quantité}{Durée}`$ où :

- __BP__ : Bande passante en bits/seconde
- __Quantité__ : Nombre de bits transférés
- __Durée__ : Durée en seconde

### Document 4 - Des clients et des serveurs

Sur un réseau, les machines échangent des données à l’aide de requêtes formulées par des programmes. Les machines émettant ces requêtes sont appelés des clients et ceux qui répondent, des serveurs.

![Architecture Client-Serveur, Source : Delagrave](./assets/client-serveur.png)

Lorsqu’on envoie depuis son ordinateur, celui-ci est position de client. Il envoie une requête à un serveur pour qu’il expédie le courrier vers un autre serveur. Le destinateur, dont l’ordinateur est aussi un client, envoie alors une requête à ce dernier pour récupérer le courriel.

## Questions

1. À l’aide des documents 1 et 2, comment expliquer l’évolution du trafic sur internet ? Comment pensez vous qu’il évoluera dans les années à venir ? Justifier votre réponse.
   ```txt
   
   ```
2. En considérant qu’un film en haute définition peut être stocké dans 4 Go, évaluer l’équivalent en nombre de films par mois le trafic mensuel en 2017, 2020 et prévu en 2022.
   ```txt
   
   
   ```
3. À partir du document 2, quelle est la bande passante moyenne en réception de données et la bande passante moyenne en envoi de données ?
   ```txt
   
   ```
4. À partir des documents 2 et 3, vous devez télécharger un document de 4 Go. Combien de temps cela va-t-il prendre en fonction de la connectique ?
   ```txt
   - BP = quantité / durée => durée = quantité / BP
   - Modem : 
   - Fibre :
     - 
     -
   - ADSL :
     -
     -
   - Réseau cables urbains :
   - 4G :
     - 
     -
   - 5G :
     -
     -
   - Satellite :
   ```
5. Pour envoyer ce même document, en ADSL, depuis votre ordinateur sur un autre serveur, le temps sera-t-il le même que pour le télécharger ? Justifier votre réponse.
   ```txt
   
   
   
   
   ```
6. À l’aide du document 4, si vous utilisez un moteur de recherche pour obtenir des informations quel peut être le client ? le serveur ?
   ```txt
   
   ```
   
7. Schématiser le réseau de votre domicile (hôtes, connectiques)
   ```txt
   
   
   
   
   
   ```
